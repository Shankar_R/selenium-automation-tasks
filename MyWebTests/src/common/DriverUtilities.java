package common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverUtilities {
	public static String ctError = "Error";
	public static String ctHide = "Hide";
	public static String ctTimeout = "Timeout";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}
	private static void initilizeChromeDriverPath(){
		//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir" + "\\drivers\\chromedriver.exe"));
		System.setProperty("webdriver.chrome.driver","D:\\Nectar_Automation\\code-base\\selenium-code-base\\MyWebTests\\drivers\\chromedriver.exe");
	}
	public ChromeDriver getChromeDriver(){
		
		initilizeChromeDriverPath();
		return new ChromeDriver(); 
	}
	public static boolean doElementKeysAction(WebElement elementToEnter,String value,String objectname){
		
		try
		{
			if(elementToEnter.isDisplayed()){
				elementToEnter.clear();
				elementToEnter.click();
				elementToEnter.sendKeys(value);
				
				System.out.println("Able to enter the " + value + " in the " + objectname + " object.");
				return true;
				
			}else{
				System.err.println( objectname + " object is not visisble");
			}
		}catch(Exception e){
			System.err.println("not able to enter the " + value + " in the " + objectname + " object. Error is : " + e.toString());
			
		}		
		return false;
	}
	public static boolean doElementClickAction(WebElement elementToEnter,String objectname){
		
		try
		{
			elementToEnter.click();
			System.out.println("Able to click the " + objectname + " object.");
			return true;
		}catch(Exception e){
			System.err.println("not able to click the " + objectname + " object. Error is : " + e.toString());
		}		
		return false;
	}
	public static boolean doSelectElementDropDown(WebElement elementToSelect,String value,String objectname){
		
		try
		{
			Select selObject = new Select(elementToSelect);
			selObject.selectByValue(value);
			System.out.println("Able to select the " + value + " in the " + objectname + " object.");
			return true;
		}catch(Exception e){
			System.err.println("not able to select the " + value + " in the " + objectname + " object");
		}		
		return false;
	}
	public static WebElement findElement(WebElement parentElement,String xpath_value){
		
		WebElement expectedObject = null;
		try
		{
			expectedObject = parentElement.findElement((By.xpath(xpath_value)));
			
			if(expectedObject == null){
				System.err.println("object not found for xpath " + xpath_value);
			}
			
		}catch(Exception e){
			System.err.println("object not found for xpath " + xpath_value);
		}
		return expectedObject;
	}
public static List<WebElement> findElements(WebElement parentElement,String xpath_value){
		
		List<WebElement> expectedObject = null;
		try
		{
			expectedObject = parentElement.findElements((By.xpath(xpath_value)));
			
			if(expectedObject == null){
				System.err.println("object not found for xpath " + xpath_value);
			}
			
		}catch(Exception e){
			System.err.println("object not found for xpath " + xpath_value);
		}
		return expectedObject;
	}
	public static WebElement getElement(String xpath_value,WebDriverWait objWDW){
		
		WebElement expectedObject = null;
		try
		{
			expectedObject = objWDW.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath_value)));
			
			if(expectedObject == null){
				
				System.err.println("object not found for xpath " + xpath_value);
			}else{
				if(expectedObject.isDisplayed()){
					return objWDW.until(ExpectedConditions.visibilityOf(expectedObject));
				}
			}
			
		}catch(Exception e){
			System.err.println("object not found for xpath " + xpath_value);
		}
		return null;
	}
	public static List<WebElement> getElements(String xpath_value,WebDriverWait objWDW){
		
		List<WebElement> expectedObject = null;
		try
		{
			expectedObject = objWDW.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath_value)));
			
			if(expectedObject == null){
				System.err.println("objects not found for xpath " + xpath_value);
			}
			
		}catch(Exception e){
			System.err.println("objects not found for xpath " + xpath_value);
		}
		return expectedObject;
	}
	public static boolean isElementExists(WebDriverWait wWait,String xpath){
		boolean isExists = false;
		try{
			if(wWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath))) != null){
				isExists = true;	
			}
		}catch(TimeoutException te){
			if(wWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)))){
				isExists = false;
			}
		}
		return isExists;
	}
	public static boolean switchMainFrame(WebDriver wDriver,String xpathofnewFrame,WebDriverWait wWait){
		if(wDriver.switchTo().frame(getElement(xpathofnewFrame, wWait)) != null){
			return true;
		}
		return false;
	}
	public static boolean switchBacktoMainFrame(WebDriver wDriver){
		if(wDriver.switchTo().defaultContent() != null){
			return true;
		}
		return false;
	}
}
