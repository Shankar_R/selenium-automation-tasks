package common;


public class ObjectRepository {
	
	//load form
	public static String regForm = "//*[@class='ajaxsubmit']";
	//registration form
	public static String regForm_txt_name = "//*[@type='text' and @name='name']";
	public static String regForm_txt_phone = "//*[@type='tel' and @name='phone']";
	public static String regForm_txt_email = "//*[@type='email' and @name='email']";
	public static String regForm_lst_country = "//*[@id='load_form']/fieldset[4]/select";
	public static String regForm_txt_city = "//*[@type='text' and @name='city']";
	public static String regForm_txt_username = "//*[@id='load_form']/fieldset[6]/input";
	public static String regForm_pwd_password = "//*[@id='load_form']/fieldset[7]/input";
	public static String regForm_btn_submit = "//*[@class='ajaxsubmit']//*[@class='button' and @value='Submit']";
	public static String regForm_alert = "//*[@id='load_form']/*[@id='alert']";
	public static String regForm_SingIn = "//*[@class='fancybox' and @href='#login'] ";
	//login details
	public static String loginForm = "//*[@class='ajaxlogin']";
	public static String loginForm_txt_username = "//*[@class='ajaxlogin']/fieldset/input[@name='username']";
	public static String loginForm_pwd_password = "//*[@class='ajaxlogin']/fieldset/input[@name='password']";
	public static String loginForm_btn_submit = "//*[@class='ajaxlogin']/div/div/*[@type='submit' and @class='button']";
	public static String loginForm_alert = "//div[@id='login']/*[@id='alert1']";
	//box
	public static String mainForm_allboxes = "//div[@class='row']//li";
	public static String getSectionBoxs(String sectionName){
		return 	"//div[@class='row']//h1[text()='" + sectionName + "']/parent::div//li";
	}
	public static String getBoxLink(String boxName){
		return "//div[@class='row']//li//h2[text()='" + boxName + "']/parent::a";
	}
	public static String getTabLink(String tabName){
		return "//a[text()='" + tabName + "']";
	}
	public static String getDemoFrame(String link){
		return "//div[contains(@id,'" + link + "')]//iframe";
	}
	public static String selectCountry_lst = "//select";
	public static String enterCountry_lst = "//select[@id='combobox']";
	public static String enterCountry_dropdown_text = "//span[@class='custom-combobox']//input";
	public static String enterCountry_dropdown_lst = "//ul[@id='ui-id-1']";
	
}