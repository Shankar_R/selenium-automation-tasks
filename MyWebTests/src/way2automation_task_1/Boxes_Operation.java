package way2automation_task_1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.DriverUtilities;
import common.ObjectRepository;

public class Boxes_Operation {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","D:\\Nectar_Automation\\code-base\\selenium-code-base\\MyWebTests\\drivers\\chromedriver.exe");
		WebDriverWait webWait =  new WebDriverWait(new ChromeDriver(), 20);		
		getAllBoxesCount(webWait);
	}
	public static int getAllBoxesCount(WebDriverWait wWait){
		
		List<WebElement> foundElements = DriverUtilities.getElements(ObjectRepository.mainForm_allboxes,wWait);
		
		if(foundElements != null){
			System.out.println("Total boxes found " +  foundElements.size() + " for the xpath " + ObjectRepository.mainForm_allboxes);
			return foundElements.size();
		}else{
			System.err.println("no boxes found");
		}
		return 0;
		
	}
	public static int getAllBoxesCountFromSection(WebDriverWait wWait,String secName){
		
		List<WebElement> foundElements = DriverUtilities.getElements(ObjectRepository.getSectionBoxs(secName),wWait);
		
		if(foundElements != null){
			System.out.println("Total boxes found " +  foundElements.size() + " for the section " + secName);
			return foundElements.size();
		}else{
			System.err.println("no boxes found");
		}
		return 0;
		
	}
	public static boolean openBox(WebDriverWait wWait,String boxName){
		if(DriverUtilities.doElementClickAction(DriverUtilities.getElement(ObjectRepository.getBoxLink(boxName), wWait), boxName)){
			return true;
		}
		return false;
	}
	public static boolean dropDownBox(WebDriver webDriver, WebDriverWait wWait,String selectValue,String searchText){
		String boxName = "Dropdown";
		String selectCountry = "Select Country";
		String enterCountry = "Enter Country";
		if(openBox(wWait, boxName)){
			//Select Country action
			WebElement SCboxLink = DriverUtilities.getElement(ObjectRepository.getTabLink(selectCountry), wWait);
			if(DriverUtilities.doElementClickAction(SCboxLink, selectCountry)){
				String hrefOfLink = SCboxLink.getAttribute("href");
				hrefOfLink = hrefOfLink.substring(hrefOfLink.length() - 5, hrefOfLink.length());
				
				//Switch to new iframe to access the elements inside that
				if(DriverUtilities.switchMainFrame(webDriver, ObjectRepository.getDemoFrame(hrefOfLink), wWait)){
					try{
						DriverUtilities.doSelectElementDropDown(DriverUtilities.getElement(ObjectRepository.selectCountry_lst,wWait), selectValue, selectCountry);
					}catch(Exception e){
						System.err.println("error occured during enter country operation. " + e.getLocalizedMessage());
					}finally{
						//back to main
						DriverUtilities.switchBacktoMainFrame(webDriver);
					}
				}
				
			}
			//Enter Country action
			WebElement ECboxLink = DriverUtilities.getElement(ObjectRepository.getTabLink(enterCountry), wWait);
			if(DriverUtilities.doElementClickAction(ECboxLink, enterCountry)){
				String hrefOfLink = ECboxLink.getAttribute("href");
				hrefOfLink = hrefOfLink.substring(hrefOfLink.length() - 4, hrefOfLink.length());
				
				if(DriverUtilities.switchMainFrame(webDriver, ObjectRepository.getDemoFrame(hrefOfLink), wWait)){
				
					try{
						WebElement EC_dropdown_text = DriverUtilities.getElement(ObjectRepository.enterCountry_dropdown_text,wWait);
						
						if(DriverUtilities.doElementKeysAction(EC_dropdown_text,searchText,"Select - Text")){
							
							if(DriverUtilities.isElementExists(wWait, ObjectRepository.enterCountry_dropdown_lst)){
								
								WebElement lstObject = DriverUtilities.getElement(ObjectRepository.enterCountry_dropdown_lst, wWait);
								
								List<WebElement> itemsintheList = lstObject.findElements(By.tagName("li"));
								
								if(itemsintheList.size() > 0){
									
									for (WebElement lItem : itemsintheList) {
										System.out.println(lItem.getAttribute("textContent"));
										if(lItem.getAttribute("textContent").equalsIgnoreCase(selectValue)){
											if(DriverUtilities.doElementClickAction(lItem,selectValue))
											{
												break;
											}
										}
									}
								}else{
									System.err.println("List count is zero");
								}
							}
						}
					}catch(Exception e){
						System.err.println("error occured during enter country operation. " + e.getLocalizedMessage());
					}finally{
						//back to main
						DriverUtilities.switchBacktoMainFrame(webDriver);
					}
				}
				
			}
			
		}
		return false;
		
	}
	
}
