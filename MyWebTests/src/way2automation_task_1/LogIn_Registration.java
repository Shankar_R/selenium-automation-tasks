package way2automation_task_1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.DriverUtilities;
import common.ObjectRepository;

public class LogIn_Registration {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","D:\\Nectar_Automation\\code-base\\selenium-code-base\\MyWebTests\\drivers\\chromedriver.exe");
		ChromeDriver chromeDriver =  new ChromeDriver();
		
		WebDriverWait webWait =  new WebDriverWait(chromeDriver, 20);
		
		doNavigation(chromeDriver);
		if(!doRegistorForm(webWait,"testuser","9874563210","test@gmail.com","India","TamilNadu","testuser_craete","test@testuser",false,20)){
			doLogin(webWait,"shankar", "shankar", false, 30);
		}
	}
	
	public static void doNavigation(WebDriver webDriver){
		webDriver.get("http://way2automation.com/way2auto_jquery/index.php");
		webDriver.manage().window().maximize();
			
	}
	public static boolean doRegistorForm(WebDriverWait webWait, String name,String phone,String email,String country,String city,String username,String password,boolean isErrorValidation,int timeOutinSec){
		WebElement mainForm = DriverUtilities.getElement(ObjectRepository.regForm, webWait);
		
		if(mainForm != null){
			//fill the in main form
			DriverUtilities.doElementKeysAction(DriverUtilities.getElement(ObjectRepository.regForm_txt_name, webWait), name, "Name");
			DriverUtilities.doElementKeysAction(DriverUtilities.getElement(ObjectRepository.regForm_txt_phone, webWait), phone, "Phone");
			DriverUtilities.doElementKeysAction(DriverUtilities.getElement(ObjectRepository.regForm_txt_email, webWait), email, "Email");
			DriverUtilities.doSelectElementDropDown(DriverUtilities.getElement(ObjectRepository.regForm_lst_country, webWait), country, "Contry");
			DriverUtilities.doElementKeysAction(DriverUtilities.getElement(ObjectRepository.regForm_txt_city, webWait), city, "City");
			DriverUtilities.doElementKeysAction(DriverUtilities.getElement(ObjectRepository.regForm_txt_username, webWait),username,"UserName");
			DriverUtilities.doElementKeysAction(DriverUtilities.getElement(ObjectRepository.regForm_pwd_password, webWait), password, "Password");
			DriverUtilities.doElementClickAction(DriverUtilities.getElement(ObjectRepository.regForm_btn_submit, webWait), "Submit");
			
			String response = waitforRegFormtoHide(webWait,ObjectRepository.regForm,ObjectRepository.regForm_alert,isErrorValidation,timeOutinSec);
			
			if(response.matches(DriverUtilities.ctHide)){
				System.out.println("Registration has completed for the user " + name);
				return true;
			}else if(response.matches(DriverUtilities.ctError)){
				if(isErrorValidation){
					System.out.println("Registration has the error message as expected.");
					return true;
				}else{
					System.err.println("Registration is having the error message.");
				}
			}else if(response.matches(DriverUtilities.ctTimeout)){
				System.err.println("Registration loads takes more hence failed");
			}	
			//clear the registration if exists
			//currently no way of closing the dialog
		}else{
			System.err.println("Registration page loads takes more hence failed");
		}
		return false;
	}
		
		
	
	public static boolean doLogin(WebDriverWait webWait,String username,String password,boolean isErrorValidation,int timeOutinSec){
		WebElement mainForm = DriverUtilities.getElement(ObjectRepository.regForm, webWait);
		
		if(mainForm != null){
			//click Sign in to show up the login window
			DriverUtilities.doElementClickAction(DriverUtilities.getElement(ObjectRepository.regForm_SingIn, webWait), "Sign In");
			WebElement loginForm = DriverUtilities.getElement(ObjectRepository.loginForm, webWait);
			
			if(loginForm != null){
				//fill the in main form
				DriverUtilities.doElementKeysAction(DriverUtilities.getElement(ObjectRepository.loginForm_txt_username, webWait), username, "User name");
				DriverUtilities.doElementKeysAction(DriverUtilities.getElement(ObjectRepository.loginForm_pwd_password, webWait), password, "Password");
				
				DriverUtilities.doElementClickAction(DriverUtilities.getElement(ObjectRepository.loginForm_btn_submit, webWait), "Submit");
				
				String response = waitforRegFormtoHide(webWait,ObjectRepository.loginForm,ObjectRepository.loginForm_alert,false,timeOutinSec);
				
				if(response.matches(DriverUtilities.ctHide)){
					System.out.println("Login has completed for the user " + username);
					return true;
				}else if(response.matches(DriverUtilities.ctError)){
					if(isErrorValidation){
						System.out.println("Login has the error message as expected.");
						return true;
					}else{
						System.err.println("Login is having the error message.");
					}
				}else if(response.matches(DriverUtilities.ctTimeout)){
					System.err.println("Login loads takes more hence failed");
				}	
			}else{
				System.err.println("Login page loads takes more hence failed");
			}
			//clear the registration if exists
			//currently no way of closing the dialog
		}else{
			System.err.println("Registration page loads takes more hence failed");
		}
		return false;
	}
	private static String waitforRegFormtoHide(WebDriverWait webWait,String mainFormxpath,String xpathofAlert,boolean isErrorValidation,int timeOutinSeconds){

		long startTime = System.currentTimeMillis();
		long timeout = timeOutinSeconds * 1000;
		WebElement errObject = null;
		
		while(!DriverUtilities.isElementExists(webWait,mainFormxpath) || (System.currentTimeMillis() - startTime) < timeout){
			
			if(!DriverUtilities.isElementExists(webWait,mainFormxpath))
			{
				return DriverUtilities.ctHide;
			}else{
				if(DriverUtilities.isElementExists(webWait,xpathofAlert)){
					errObject = webWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathofAlert)));
					if(errObject.isDisplayed()){
						String errMessage = errObject.getAttribute("innerText");
						if(errMessage.length() > 0){
							if(isErrorValidation){
								System.out.println(errMessage + " error message is displayed");
								return DriverUtilities.ctError;
							}else{
								System.err.println(errMessage + " error message is displayed");
								return DriverUtilities.ctError;
							}
						}
					}
				}
			}
		}
		return DriverUtilities.ctTimeout;
	}
	
}
