package runner;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import way2automation_task_1.Boxes_Operation;
import way2automation_task_1.LogIn_Registration;
public class Runner {

	private static ChromeDriver chromeDriver;
	private static WebDriverWait webWait;
	
	public static void main(String[] args) {
		
		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
		
		chromeDriver = new ChromeDriver();
		
		webWait =  new WebDriverWait(chromeDriver, 10);
		LogIn_Registration.doNavigation(chromeDriver);

		if(LogIn_Registration.doLogin(webWait,"shankar", "shankar", false, 30)){
			Boxes_Operation.getAllBoxesCount(webWait);
			Boxes_Operation.getAllBoxesCountFromSection(webWait, "Alert");
			Boxes_Operation.getAllBoxesCountFromSection(webWait, "Registration");
			Boxes_Operation.getAllBoxesCountFromSection(webWait, "Dynamic Elements");
			Boxes_Operation.getAllBoxesCountFromSection(webWait, "Frames and Windows");
			Boxes_Operation.getAllBoxesCountFromSection(webWait, "Widget");
			Boxes_Operation.getAllBoxesCountFromSection(webWait, "Interaction");
			Boxes_Operation.dropDownBox(chromeDriver,webWait, "India", "Ind");
		}
		
		chromeDriver.close();
	}

}
