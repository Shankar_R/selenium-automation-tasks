package automation.framework.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.management.RuntimeErrorException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import automation.framework.constants.Constants;
import automation.framework.testdata.TestDataRow;
import automation.framework.testdata.VCell;

public class ExcelObject {
	private String strExcelPath;
	private Workbook wb_Main;
	private Sheet s_Current;

	public ExcelObject(String excelPath) throws RuntimeErrorException, FileNotFoundException, IOException {
		if (excelPath.endsWith(".xls")) {
			this.strExcelPath = excelPath;
			prepareExcelObject();
		} else {
			throw new RuntimeErrorException(null, "This suite only supports XLS file format");
		}

	}

	private void prepareExcelObject() throws FileNotFoundException, IOException {
		this.setMainWorkbook(new HSSFWorkbook(new FileInputStream(this.strExcelPath)));
	}

	public Workbook getMainWorkbook() {
		return wb_Main;
	}

	private void setMainWorkbook(Workbook wb_Main) {
		this.wb_Main = wb_Main;
	}

	public void setCurrentSheet(String strSheetName) {
		setCurrentSheet(wb_Main.getSheet(strSheetName));
	}

	public Sheet getCurrentSheet() {
		return this.s_Current;
	}

	public void setCurrentSheet(Sheet sheetObject) {
		this.s_Current = sheetObject;
	}

	public TestDataRow getCurrentTestCaseDataRow(String strTestCaseID) {
		Sheet sheetObject = getCurrentSheet();
		int rowCount = sheetObject.getLastRowNum() - sheetObject.getFirstRowNum();
		int colNum = getColumnNumber(sheetObject, Constants.defaultTCColumnName);
		TestDataRow tdr = new TestDataRow(strTestCaseID);

		for (int r = Constants.defaultRowNumber + 1; r <= rowCount; r++) {
			String curRowValue = getCellValueAsString(sheetObject.getRow(r).getCell(colNum));

			if (strTestCaseID.equalsIgnoreCase(curRowValue)) {
				int colCount = sheetObject.getRow(r).getLastCellNum();
				for (int c = 0; c < colCount; c++) {
					String colName = getCellValueAsString(sheetObject.getRow(Constants.defaultRowNumber).getCell(c));
					String colVal = getCellValueAsString(sheetObject.getRow(r).getCell(c));
					tdr.setCellValues(new VCell(colName, colVal));
				}
			}
		}
		return tdr;
	}
	
	public String[][] getTestCaseIDs() {
		Sheet sheetObject = getCurrentSheet();
		int rowCount = sheetObject.getLastRowNum() - sheetObject.getFirstRowNum();
		int colNum = getColumnNumber(sheetObject, Constants.defaultTCColumnName);
		int in = -1;
		String[][] returnStr = new String[rowCount][1];
		
		for (int r = Constants.defaultRowNumber + 1; r <= rowCount; r++) {
			in++;
			returnStr[in][0] = getCellValueAsString(sheetObject.getRow(r).getCell(colNum));
		}
		return returnStr;
	}
	
	public int getColumnNumber(Sheet sObject, String strColumnName) {
		for (int c = 0; c < sObject.getRow(Constants.defaultRowNumber).getLastCellNum(); c++) {
			Cell curCellValue = sObject.getRow(Constants.defaultRowNumber).getCell(c);
			String strCellValue = getCellValueAsString(curCellValue);
			if (strColumnName.equalsIgnoreCase(strCellValue)) {
				return c;
			}
		}
		return -1;
	}

	public static String getCellValueAsString(Cell cell) {
		String strCellValue = "";
		if (cell != null) {
			strCellValue = cell.getStringCellValue();
		}
		return strCellValue;
	}
}
