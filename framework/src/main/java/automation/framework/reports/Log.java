package automation.framework.reports;

import org.testng.Reporter;

public class Log {

	private static String tcName = "";

	public Log(String TestCaseName) {
		setTcName(TestCaseName);
		System.out.println(TestCaseName + " Starts here:");
		System.out.println("#########################################################################");
	}

	public void Pass(String expected) {
		System.out.println("PASS: " + expected);
		Reporter.log("PASS: " + expected);

	}

	public void Fail(String expected) {
		System.err.println("FAIL: " + expected);
		Reporter.log("FAIL: " + expected);
	}

	public void Warning(String expected) {
		System.out.println("WARNING: " + expected);
		Reporter.log("WARNING: " + expected);
	}

	public void Done(String expected) {
		System.out.println("DONE: " + expected);
		Reporter.log("DONE: " + expected);
	}

	@SuppressWarnings("unused")
	private String getTcName() {
		return tcName;
	}

	public static void setTcName(String tcName) {
		Log.tcName = tcName;
	}

}
