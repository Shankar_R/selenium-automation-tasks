package automation.framework.useraction;

import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import automation.framework.reports.Log;

public class DoAction {

	public static boolean EnterText(WebDriverWait wDriverWait, By webEleLocator, String strValue, Log log,String friendlyName) {
		try {
			WebElement txtObject = wDriverWait.until(ExpectedConditions.presenceOfElementLocated(webEleLocator));
			if (txtObject != null) {
				txtObject.sendKeys(strValue);
				if (strValue.equals(txtObject.getAttribute("value")) || strValue.equals(txtObject.getText())) {
					log.Done(friendlyName + " Textbox entered as " + strValue);
					return true;
				} else {
					log.Fail(friendlyName + " Textbox entered as " + txtObject.getText() + " instead of " + strValue);
					return false;
				}
			}
		} catch (Exception e) {
			log.Fail(friendlyName + " Textbox not available to enter. Error occured" + e.getMessage());
		}
		log.Fail(friendlyName + " Textbox not available to enter");
		return false;
	}

	public static boolean Click(WebDriver wDriver, WebDriverWait wDriverWait, Object webEleLocator,
			Log log,String friendlyName) {
		try {
			WebElement txtObject = null;

			if (webEleLocator instanceof By) {
				txtObject = wDriverWait.until(ExpectedConditions.presenceOfElementLocated((By) webEleLocator));
			} else if (webEleLocator instanceof WebElement) {
				txtObject = (WebElement) webEleLocator;
			}

			if (txtObject != null) {

				try {
					txtObject.click();
					log.Done(friendlyName + " Button clicked");
					return true;
				} catch (ElementNotVisibleException e) {
					JavascriptExecutor js = (JavascriptExecutor) wDriver;
					js.executeScript("arguments[0].click();", txtObject);
					log.Done(friendlyName + " Button is invisible trying with JavascriptExecutor to do the action");
					return true;
				}

			}
		} catch (Exception e) {
			log.Fail(friendlyName + " Button not available to enter. Error occured" + e.getMessage());
		}
		log.Fail(friendlyName + " Button not available to click");
		return false;
	}

	public static void Select(WebDriver wDriver, WebDriverWait wDriverWait, Object webEleLocator, String valueToSelect,
			boolean usingManual, Log log,String friendlyName) {
		WebElement selObject = null;

		if (webEleLocator instanceof By) {
			selObject = wDriverWait.until(ExpectedConditions.presenceOfElementLocated((By) webEleLocator));
		} else if (webEleLocator instanceof WebElement) {
			selObject = (WebElement) webEleLocator;
		}

		Select selObj = new Select(selObject);

		String[] arrOFOptions = valueToSelect.split(Pattern.quote("|"));

		if (arrOFOptions.length == 1) {
			selObj.selectByVisibleText(arrOFOptions[0]);
			log.Done(friendlyName + " List selected as " + arrOFOptions[0]);
		} else {
			if (selObj.isMultiple()) {
				if (usingManual) {
					Actions builder = new Actions(wDriver);

					builder.sendKeys(Keys.CONTROL).clickAndHold();

					for (int i = 0; i < arrOFOptions.length; i++) {
						try {
							builder.click(getOptionElement(selObj, arrOFOptions[i]));
							log.Done(friendlyName + " List selected as " + arrOFOptions[i]);
						} catch (NullPointerException npe) {
							log.Fail(arrOFOptions[i] + "is not available to select");
						}
					}

					builder.sendKeys(Keys.CONTROL).release();
					builder.build().perform();
				} else {
					for (int i = 0; i < arrOFOptions.length; i++) {
						selObj.selectByVisibleText(arrOFOptions[i]);
						log.Done(friendlyName + " List selected as " + arrOFOptions[i]);
					}
				}
			}
		}

		/*
		 * 
		 * selObj.selectByValue(valueToSelect);
		 * 
		 * if (valueToSelect.equals(selObject.getText())){ log.Done(friendlyName +
		 * " List selected as " + valueToSelect); return true; } else {
		 * log.Fail(friendlyName + " List not selected as " + selObject.getText() +
		 * " instead of " + valueToSelect); return false; }
		 */

		/*
		 * if(selObject.isDisplayed()) { List<WebElement> optionsList =
		 * selObject.findElements(By.tagName("option"));
		 * 
		 * for (WebElement webElement : optionsList) {
		 * if(webElement.getText().equals(valueToSelect) ||
		 * webElement.getAttribute("value").equals(valueToSelect)) { return
		 * Click(wDriver, wDriverWait, webElement, valueToSelect); } } }
		 */

	}

	private static WebElement getOptionElement(Select selElement, String value) {

		for (WebElement optionObj : selElement.getOptions()) {
			if (optionObj.getText().equals(value)) {
				return optionObj;
			}
		}
		return null;
	}
}
