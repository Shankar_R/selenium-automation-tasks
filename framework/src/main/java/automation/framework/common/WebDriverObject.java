package automation.framework.common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import automation.framework.constants.Constants;
import setup.environment.PrepareEnvironment;

public class WebDriverObject {

	public WebDriver webDrv;
	public WebDriverWait webDrvWait;

	public WebDriverObject() {
		String browName = PrepareEnvironment.getBrowserName();
		long seconds = PrepareEnvironment.getWebDriverWaitTimeOUtSeconds();
		
		switch (browName) {
		case Constants.CHROME:
			webDrv = getChromeDriver();
			break;
		default:
			webDrv = getFireFoxDriver();
			break;
		}
		webDrv.manage().timeouts().pageLoadTimeout(PrepareEnvironment.getPageLoadTimeOUtSeconds(), TimeUnit.SECONDS);
		
		if(PrepareEnvironment.isDeleteCoockies()) {
			webDrv.manage().deleteAllCookies(); 
		}
		
		webDrv.manage().window().maximize();
		webDrvWait = new WebDriverWait(webDrv, seconds);
	}

	private WebDriver getChromeDriver() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/resources/drivers/chromedriver.exe");
		WebDriver chromeDrv = new ChromeDriver();
		return chromeDrv;
	}

	private WebDriver getFireFoxDriver() {
		System.setProperty("webdriver.firefox.driver", System.getProperty("user.dir") + "/src/resources/drivers/geckodriver.exe");
		WebDriver chromeDrv = new FirefoxDriver();
		return chromeDrv;
	}


	public WebDriver getDriver() {
		return this.webDrv;
	}

	public WebDriverWait getWebDriverWait() {
		return this.webDrvWait;
	}

	public static void main(String[] args) {
		// Basic setup
		/*
		 * wDriver.manage().deleteAllCookies(); wDriver.manage().window().maximize();
		 * 
		 */
	}
}
