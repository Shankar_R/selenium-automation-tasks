package automation.framework.testdata;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.management.RuntimeErrorException;

import automation.framework.excel.ExcelObject;
import setup.environment.PrepareEnvironment;

public abstract class TestCase {
	private TestDataRow testdata;
	private String TestCaseID;
	private String TestCaseDescription;
	private String EnvironmentDetails;
	private boolean RunFlag;
	
	public TestCase(String tcid){
		setTestCaseID(tcid);
	}
	public TestCase(String tcid,String tcd){
		this(tcid);
		setTestCaseDescription(tcd);
	}
	public TestCase(String excelFileName,String sheetName,String tcid) throws RuntimeErrorException, FileNotFoundException, IOException{
		this(tcid);
		ExcelObject exlObj = new ExcelObject(PrepareEnvironment.getTestDataMainPath() + excelFileName);
		
		exlObj.setCurrentSheet(sheetName);
		setTestdata(exlObj.getCurrentTestCaseDataRow(tcid));
	}
	
	public String getTestData(String colName) {
		return getTestdataRow().getTestData(colName);
	}
	
	public TestDataRow getTestdataRow() {
		return testdata;
	}
	public void setTestdata(TestDataRow testdata) {
		this.testdata = testdata;
	}
	public String getTestCaseID() {
		return TestCaseID;
	}
	public void setTestCaseID(String testCaseID) {
		TestCaseID = testCaseID;
	}
	public String getTestCaseDescription() {
		return TestCaseDescription;
	}
	public void setTestCaseDescription(String testCaseDescription) {
		TestCaseDescription = testCaseDescription;
	}
	public String getEnvironmentDetails() {
		return EnvironmentDetails;
	}
	public void setEnvironmentDetails(String environmentDetails) {
		EnvironmentDetails = environmentDetails;
	}
	public boolean isRunFlag() {
		return RunFlag;
	}
	public void setRunFlag(boolean runFlag) {
		RunFlag = runFlag;
	}
	
	
}
