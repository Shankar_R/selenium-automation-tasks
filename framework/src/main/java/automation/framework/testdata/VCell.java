package automation.framework.testdata;

public class VCell {
	private String columnName;
	private String columnValue;
	
	public VCell(String colName,String colValue){
		setColumnName(colName);
		setColumnValue(colValue);
	}

	public String getColumnValue() {
		return columnValue;
	}

	public void setColumnValue(String columnValue) {
		this.columnValue = columnValue;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
}
