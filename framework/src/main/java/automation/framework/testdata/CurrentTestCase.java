package automation.framework.testdata;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.management.RuntimeErrorException;

public class CurrentTestCase extends TestCase{

	public CurrentTestCase(String excelFileName,String sheetName, String tcid) throws RuntimeErrorException, FileNotFoundException, IOException {
		super(excelFileName,sheetName,tcid);
	}
}
