package automation.framework.testdata;

import java.util.ArrayList;
import java.util.List;

public class TestDataRow {
	private String TestCaseID;
	private List<VCell> cellValues;

	public TestDataRow(String strTestCaseID) {
		setTestCaseID(strTestCaseID);
		cellValues = new ArrayList<VCell>();
	}

	public void addCell(VCell cellVal) {
		setCellValues(cellVal);
	}

	public String getTestCaseID() {
		return TestCaseID;
	}

	public void setTestCaseID(String testCaseID) {
		TestCaseID = testCaseID;
	}

	public List<VCell> getCellValues() {
		return cellValues;
	}

	public void setCellValues(VCell cellValue) {
		this.cellValues.add(cellValue);
	}

	public String getTestData(String ColumnName) {
		for (VCell vCell : cellValues) {
			if (ColumnName.equalsIgnoreCase(vCell.getColumnName())) {
				return vCell.getColumnValue();
			}
		}
		return "";
	}
}
