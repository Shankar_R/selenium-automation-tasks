package setup.environment;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import automation.framework.constants.Constants;

public class PrepareEnvironment {
	private static String BrowserName = Constants.FIREFOX;
	private static long WebDriverWaitTimeOUtSeconds = 20;
	private static long PageLoadTimeOUtSeconds = 15;
	private static String LogsPath = Constants.defaultLogsPath;
	private static boolean deleteCoockies = false;
	private static String TestDataMainPath;
	
	@BeforeSuite
	@Parameters({ "BroweserName","WDWTimeOutInSec","PageTimeOut","LogsPath","DeleteCookies","TestDataMainPath"})
	public void beforeSuite(String BrowserName,long WDWTimeOutInSec,long PageTimeOut,String LogsPath,boolean DeleteCookies,String TestDataMainPath) {
		setBrowserName(BrowserName);
		setWebDriverWaitTimeOUtSeconds(WDWTimeOutInSec);
		setPageLoadTimeOUtSeconds(PageTimeOut);
		setLogsPath(LogsPath);
		setDeleteCoockies(DeleteCookies);
		setTestDataMainPath(TestDataMainPath);
		
	}
	@Test
	public void startFramewoek() {
		System.out.println("Framework stated");
	}
	public static String getBrowserName() {
		return BrowserName;
	}

	public static void setBrowserName(String browserName) {
		BrowserName = browserName;
	}

	public static long getWebDriverWaitTimeOUtSeconds() {
		return WebDriverWaitTimeOUtSeconds;
	}

	public static void setWebDriverWaitTimeOUtSeconds(long webDriverWaitTimeOUtSeconds) {
		WebDriverWaitTimeOUtSeconds = webDriverWaitTimeOUtSeconds;
	}

	public static long getPageLoadTimeOUtSeconds() {
		return PageLoadTimeOUtSeconds;
	}

	public static void setPageLoadTimeOUtSeconds(long pageLoadTimeOUtSeconds) {
		PageLoadTimeOUtSeconds = pageLoadTimeOUtSeconds;
	}

	public static String getLogsPath() {
		return LogsPath;
	}

	public static void setLogsPath(String logsPath) {
		LogsPath = logsPath;
	}

	public static boolean isDeleteCoockies() {
		return deleteCoockies;
	}

	public static void setDeleteCoockies(boolean deleteCoockies) {
		PrepareEnvironment.deleteCoockies = deleteCoockies;
	}

	public static String getTestDataMainPath() {
		return TestDataMainPath;
	}

	public static void setTestDataMainPath(String testDataMainPath) {
		TestDataMainPath = System.getProperty("user.dir") + testDataMainPath;
	}
	

}
