package seleniumeasy.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import javax.management.RuntimeErrorException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import automation.framework.common.WebDriverObject;
import automation.framework.excel.ExcelObject;
import automation.framework.reports.Log;
import automation.framework.testdata.CurrentTestCase;
import automation.framework.testdata.TestCase;
import automation.framework.useraction.DoAction;
import objectrepository.seleniumeasy.HomePage;
import objectrepository.seleniumeasy.InputForms.CheckboxesDemo;
import objectrepository.seleniumeasy.InputForms.InputFormSubmitDemo;
import objectrepository.seleniumeasy.InputForms.RadioButtonDemo;
import objectrepository.seleniumeasy.InputForms.SelectListDemo;
import objectrepository.seleniumeasy.InputForms.SimpleInputDemo;
import setup.environment.PrepareEnvironment;

public class InputFormsTest {

	WebDriver wDriver;
	WebDriverWait wDriverWait;
	JavascriptExecutor wJSExecutor;
	SimpleInputDemo OR_SimpleInputForm;
	HomePage OR_HomePage;
	CheckboxesDemo OR_CheckboxesForm;
	RadioButtonDemo OR_RadioButtonsForm;
	SelectListDemo OR_SelectListForm;
	InputFormSubmitDemo OR_InputFormSubmit;
	TestCase TC;

	private static final String fi_firstName = "FirstName";
	private static final String fi_lastName = "LastName";
	private static final String fi_Email = "Email";
	private static final String fi_Phone = "Phone";
	private static final String fi_Address = "Address";
	private static final String fi_City = "City";
	private static final String fi_State = "State";
	private static final String fi_ZipCode = "ZipCode";
	private static final String fi_Domain = "Domain";
	private static final String fi_DoYouHost = "DoYouHost";
	private static final String fi_ProjDesc = "ProjDescription";

	@BeforeTest
	public void setUP() throws RuntimeErrorException, FileNotFoundException, IOException {

		WebDriverObject wdo = new WebDriverObject();
		wDriver = wdo.getDriver();
		wDriverWait = wdo.getWebDriverWait();
		wJSExecutor = (JavascriptExecutor) wDriver;
		OR_SimpleInputForm = new SimpleInputDemo();
		OR_HomePage = new HomePage();
		OR_CheckboxesForm = new CheckboxesDemo();
		OR_RadioButtonsForm = new RadioButtonDemo();
		OR_SelectListForm = new SelectListDemo();
		OR_InputFormSubmit = new InputFormSubmitDemo();

	}

	@Test(priority = 0, enabled = true, groups = { "Welcome", "SimpleFormInput", "InputForms" })
	public void welcomeValidation() throws RuntimeErrorException, FileNotFoundException, IOException {
		TC = new CurrentTestCase("InputForm.xls", "Welcome", "TC-01-Welcome");
		Log myLog = new Log("TC-01-Welcome");

		wDriver.get(TC.getTestData("url"));
		navigateToStartUpPage(myLog);
	}

	public boolean navigateToStartUpPage(Log myLog) {
		if (DoAction.Click(wDriver, wDriverWait, OR_HomePage.btnBasicExample.getObjectSelector(), myLog,
				OR_HomePage.btnBasicExample.getObjectName())) {
			if (wDriverWait.until(ExpectedConditions
					.presenceOfElementLocated(OR_HomePage.lblBasicExamplepage.getObjectSelector())) != null) {
				myLog.Pass("First page loaded");
				return true;
			}
		}
		return false;
	}

	@Test(priority = 1, enabled = true, groups = { "SimpleFormInput", "InputForms" })
	public void SimpleFormTest() throws RuntimeErrorException, FileNotFoundException, IOException {

		TC = new CurrentTestCase("InputForm.xls", "SimpleFormDemo", "TC-02-SimpleInputDemo");
		Log myLog = new Log("TC-02-SimpleInputDemo");

		if (NavigateToScreen(TC.getTestData("mainMenu"), TC.getTestData("subMenu"),
				OR_SimpleInputForm.lblHeading.getObjectSelector(), myLog)) {

			// Single Input Field
			String firstInput = TC.getTestData("FirstInput");

			DoAction.EnterText(wDriverWait, OR_SimpleInputForm.txtEnterMessage.getObjectSelector(), firstInput, myLog,
					OR_SimpleInputForm.txtEnterMessage.getObjectName());
			DoAction.Click(wDriver, wDriverWait, OR_SimpleInputForm.btnShowMessage.getObjectSelector(), myLog,
					OR_SimpleInputForm.btnShowMessage.getObjectName());

			wDriverWait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver d) {
					return d.findElement(OR_SimpleInputForm.lblUserMessage.getObjectSelector()).getText().length() != 0;
				}
			});
			WebElement lblResult = wDriver.findElement(OR_SimpleInputForm.lblUserMessage.getObjectSelector());

			if (firstInput.equals(lblResult.getText())) {
				myLog.Pass("Your Message content is mathched with the entered content: [Value=" + firstInput + "]");
			} else {
				myLog.Fail("Your Message content is not mathched with the entered content: [Expected Value="
						+ firstInput + " Actual Value =" + lblResult.getText() + "]");
			}

			// Two Input Fields
			String a = TC.getTestData("a_value");
			String b = TC.getTestData("b_value");

			DoAction.EnterText(wDriverWait, OR_SimpleInputForm.txta.getObjectSelector(), a, myLog,
					OR_SimpleInputForm.txta.getObjectName());
			DoAction.EnterText(wDriverWait, OR_SimpleInputForm.txtb.getObjectSelector(), b, myLog,
					OR_SimpleInputForm.txtb.getObjectName());

			DoAction.Click(wDriver, wDriverWait, OR_SimpleInputForm.btnGetTotal.getObjectSelector(), myLog,
					"Get Total");

			Integer result = Integer.parseInt(a) + Integer.parseInt(b);

			final By totalResult = OR_SimpleInputForm.lblTotalaPulsb.getObjectSelector();

			wDriverWait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver d) {
					return d.findElement(totalResult).getText().length() != 0;
				}
			});
			WebElement lblTotalResult = wDriver.findElement(totalResult);
			String resultvalue = lblTotalResult.getText();
			if (String.valueOf(result).equals(resultvalue)) {
				myLog.Pass("Your Message content is mathched with the entered content: [Value=" + result + "]");
			} else {
				myLog.Fail("Your Message content is not mathched with the entered content: [Expected Value=" + result
						+ " Actual Value =" + resultvalue + "]");
			}
		}

	}

	@Test(priority = 2, enabled = true, groups = { "CheckBoxDemo", "InputForms" })
	public void CheckBoxSingleTest() throws RuntimeErrorException, FileNotFoundException, IOException {
		TC = new CurrentTestCase("InputForm.xls", "CheckboxDemo", "TC-02-CheckBoxDemo-Single");
		Log myLog = new Log("TC-02-CheckBoxDemo-Single");

		if (NavigateToScreen(TC.getTestData("mainMenu"), TC.getTestData("subMenu"),
				OR_CheckboxesForm.lblHeading.getObjectSelector(), myLog)) {

			// Single Checkbox Demo
			if (ClickCheckBox(OR_CheckboxesForm.cBokOptions.getObjectSelector(), TC.getTestData("CheckBoxName"),
					myLog)) {

				boolean islblResult = wDriverWait.until(new ExpectedCondition<Boolean>() {
					public Boolean apply(WebDriver wd) {
						return wd.findElement(OR_CheckboxesForm.lblClickOnThisBoxResult.getObjectSelector()).getText()
								.length() != 0;
					}
				});

				if (islblResult) {
					String valueofResult = wDriver
							.findElement(OR_CheckboxesForm.lblClickOnThisBoxResult.getObjectSelector()).getText();
					String expResult = TC.getTestData("SingleCHKBOXResult");

					if (expResult.equals(valueofResult)) {
						myLog.Pass("The checkbox is selected and got output as " + valueofResult);
					} else {
						myLog.Fail("The checkbox is selected and got output as " + valueofResult + " instead of "
								+ expResult);
					}
				} else {
					myLog.Fail("The checkbox result not available to validate");
				}
			}

		}

	}

	@Test(priority = 3, enabled = true, groups = { "CheckBoxDemo", "InputForms" })
	public void CheckBoxMultipleTest() throws RuntimeErrorException, FileNotFoundException, IOException {
		TC = new CurrentTestCase("InputForm.xls", "CheckboxDemo", "TC-03-CheckBoxDemo-Mulitple");
		Log myLog = new Log("TC-03-CheckBoxDemo-Mulitple");

		if (NavigateToScreen(TC.getTestData("mainMenu"), TC.getTestData("subMenu"),
				OR_CheckboxesForm.lblHeading.getObjectSelector(), myLog)) {

			// Check all and see whether all check boxes are checked
			DoAction.Click(wDriver, wDriverWait, OR_CheckboxesForm.btnCheckAll.getObjectSelector(), myLog,
					OR_CheckboxesForm.btnCheckAll.getObjectName());

			List<WebElement> arrWedEle = wDriver.findElements(OR_CheckboxesForm.cBokOptions.getObjectSelector());

			for (WebElement webElement : arrWedEle) {
				if (webElement.getText().contains(TC.getTestData("CheckBoxName"))) {
					if (isCheckBoxSelected(webElement.findElement(By.tagName("input")))) {
						myLog.Pass(webElement.getText() + " is selected");
					} else {
						myLog.Fail(webElement.getText() + " is not selected");
					}
				}
			}

			WebElement chkAll = wDriver.findElement(OR_CheckboxesForm.btnUnCheckAll.getObjectSelector());
			// Checking the button state
			if (chkAll.getAttribute("value").equals("Uncheck All")) {
				myLog.Pass("The checkbox is changed to Uncheck All");
			} else {
				myLog.Fail("The checkbox is not changed to Uncheck All");
			}

			int counter = -1;
			for (WebElement webElement : arrWedEle) {
				String tempValue = TC.getTestData("CheckBoxName");

				if (webElement.getText().contains(tempValue)) {
					DoAction.Click(wDriver, wDriverWait, webElement, myLog, webElement.getText());
					counter++;
					if (counter == 2) {
						break;
					}
				}

			}

			WebElement unchkAll = wDriver.findElement(OR_CheckboxesForm.btnCheckAll.getObjectSelector());
			// Checking the button state
			if (unchkAll.getAttribute("value").equals("Check All")) {
				myLog.Pass("The checkbox is changed to Check All");
			} else {
				myLog.Fail("The checkbox is not changed to Check All");
			}
		}

	}

	@Test(priority = 4, enabled = true, groups = { "RadioButtonDemo", "InputForms" })
	public void RadioButtonTest() throws RuntimeErrorException, FileNotFoundException, IOException {

		TC = new CurrentTestCase("InputForm.xls", "RadioButtonDemo", "TC-04-RadioButtonDemo-NotClicked");
		Log myLog = new Log("TC-04-RadioButtonDemo-NotClicked");

		if (NavigateToScreen(TC.getTestData("mainMenu"), TC.getTestData("subMenu"),
				OR_RadioButtonsForm.lblHeading.getObjectSelector(), myLog)) {

			doRadioButtonAction(OR_RadioButtonsForm.divRadioButtonDemo.getObjectSelector(),
					TC.getTestData("ButtonName"), TC.getTestData("Gender"), TC.getTestData("ExpectedResult"), myLog);

			TC = new CurrentTestCase("InputForm.xls", "RadioButtonDemo", "TC-05-RadioButtonDemo-Clicked");

			doRadioButtonAction(OR_RadioButtonsForm.divRadioButtonDemo.getObjectSelector(),
					TC.getTestData("ButtonName"), TC.getTestData("Gender"), TC.getTestData("ExpectedResult"), myLog);
		}

	}

	@Test(priority = 5, enabled = true, groups = { "RadioButtonDemo", "InputForms" })
	public void RadioGroupButtonTest() throws RuntimeErrorException, FileNotFoundException, IOException {

		TC = new CurrentTestCase("InputForm.xls", "RadioButtonDemo", "TC-06-GroupRadioButtonsDemo");
		Log myLog = new Log("TC-06-GroupRadioButtonsDemo");

		if (NavigateToScreen(TC.getTestData("mainMenu"), TC.getTestData("subMenu"),
				OR_RadioButtonsForm.lblHeading.getObjectSelector(), myLog)) {

			DoAction.Click(wDriver, wDriverWait,
					OR_RadioButtonsForm.getRaidoButtonLocator(
							OR_RadioButtonsForm.divRadioGroupButtonsDemo.getObjectSelector(), TC.getTestData("Gender")),
					myLog, TC.getTestData("Gender"));
			DoAction.Click(wDriver, wDriverWait,
					OR_RadioButtonsForm.getRaidoButtonLocator(
							OR_RadioButtonsForm.divRadioGroupButtonsDemo.getObjectSelector(), TC.getTestData("Age")),
					myLog, TC.getTestData("Age"));

			DoAction.Click(wDriver, wDriverWait,
					OR_RadioButtonsForm.getButtonLocator(
							OR_RadioButtonsForm.divRadioGroupButtonsDemo.getObjectSelector(),
							TC.getTestData("ButtonName")),
					myLog, TC.getTestData("ButtonName"));

			waitUntilLengthGreaterThanOne(OR_RadioButtonsForm.lblRadioGroupButtonDemoResult.getObjectSelector());

			WebElement lblResult = wDriver
					.findElement(OR_RadioButtonsForm.lblRadioGroupButtonDemoResult.getObjectSelector());

			String expResult = TC.getTestData("ExpectedResult");

			if (expResult.equals(lblResult.getText())) {
				myLog.Pass("Radio group button result is matched" + expResult);
			} else {
				myLog.Fail("Radio group button result is not matched: Actual [" + lblResult.getText() + "] Expected ["
						+ expResult + "]");
			}
		}
	}

	@Test(priority = 5, enabled = true, groups = { "SelectListDemo", "InputForms" })
	public void SelectListTest() throws RuntimeErrorException, FileNotFoundException, IOException {

		TC = new CurrentTestCase("InputForm.xls", "SelectListDemo", "TC-07-SelectListDemo");
		Log myLog = new Log("TC-07-SelectListDemo");

		if (NavigateToScreen(TC.getTestData("mainMenu"), TC.getTestData("subMenu"),
				OR_SelectListForm.lblHeading.getObjectSelector(), myLog)) {

			// Single select box
			DoAction.Select(wDriver, wDriverWait, OR_SelectListForm.lstDay.getObjectSelector(),
					TC.getTestData("SingleSelect"), false, myLog, TC.getTestData("SingleSelect"));
			waitUntilLengthGreaterThanOne(OR_SelectListForm.lblSelectedValue.getObjectSelector());

			matchResult(OR_SelectListForm.lblSelectedValue.getObjectSelector(),
					"Day selected :- " + TC.getTestData("SingleSelect"), "Day selected", myLog);

			// Multiple select box
			String strToSelect = TC.getTestData("MultiSelect");

			DoAction.Select(wDriver, wDriverWait, OR_SelectListForm.lstMulitSelect.getObjectSelector(), strToSelect,
					true, myLog, strToSelect);

			// First selected validation
			DoAction.Click(wDriver, wDriverWait, OR_SelectListForm.btnFirstSelected.getObjectSelector(), myLog,
					OR_SelectListForm.btnFirstSelected.getObjectName());
			String expString = "First selected option is : " + strToSelect.split(Pattern.quote("|"))[0];
			matchResult(OR_SelectListForm.lblAllSelectedValue.getObjectSelector(), expString, "First selected string",
					myLog);

			// All Selected validation
			DoAction.Click(wDriver, wDriverWait, OR_SelectListForm.btnDisplayAll.getObjectSelector(), myLog,
					OR_SelectListForm.btnDisplayAll.getObjectName());
			expString = "Options selected are : " + strToSelect.replaceAll(Pattern.quote("|"), ",");
			matchResult(OR_SelectListForm.lblAllSelectedValue.getObjectSelector(), expString, "Get All selected string",
					myLog);
		}
	}

	@Test(priority = 6, enabled = true, groups = { "InputFormSubmit", "InputForms" },dataProvider = "InputFormData")
	public void InputFormSubmit(String tcName) throws RuntimeErrorException, FileNotFoundException, IOException {

		TC = new CurrentTestCase("InputForm.xls", "InputFormDemo",tcName);
		
		Log myLog = new Log(tcName);

		if (NavigateToScreen(TC.getTestData("mainMenu"), TC.getTestData("subMenu"),
				OR_InputFormSubmit.lblHeading.getObjectSelector(), myLog)) {

			String firstName = TC.getTestData(fi_firstName);
			String LastName = TC.getTestData(fi_lastName);
			String EMail = TC.getTestData(fi_Email);
			String Phone = TC.getTestData(fi_Phone);
			String Address = TC.getTestData(fi_Address);
			String City = TC.getTestData(fi_City);
			String State = TC.getTestData(fi_State);
			String ZipCode = TC.getTestData(fi_ZipCode);
			String WebSite = TC.getTestData(fi_Domain);
			String DoyouHost = TC.getTestData(fi_DoYouHost);
			String ProjDesc = TC.getTestData(fi_ProjDesc);

			// Perform action
			DoAction.EnterText(wDriverWait, OR_InputFormSubmit.txtFirstName.getObjectSelector(), firstName, myLog,
					OR_InputFormSubmit.txtFirstName.getObjectName());
			DoAction.EnterText(wDriverWait, OR_InputFormSubmit.txtLastName.getObjectSelector(), LastName, myLog,
					OR_InputFormSubmit.txtLastName.getObjectName());
			DoAction.EnterText(wDriverWait, OR_InputFormSubmit.txtEmail.getObjectSelector(), EMail, myLog,
					OR_InputFormSubmit.txtEmail.getObjectName());
			DoAction.EnterText(wDriverWait, OR_InputFormSubmit.txtPhone.getObjectSelector(), Phone, myLog,
					OR_InputFormSubmit.txtPhone.getObjectName());
			DoAction.EnterText(wDriverWait, OR_InputFormSubmit.txtAddress.getObjectSelector(), Address, myLog,
					OR_InputFormSubmit.txtAddress.getObjectName());
			DoAction.EnterText(wDriverWait, OR_InputFormSubmit.txtCity.getObjectSelector(), City, myLog,
					OR_InputFormSubmit.txtCity.getObjectName());
			
			DoAction.Select(wDriver, wDriverWait, OR_InputFormSubmit.lstState.getObjectSelector(), State, false, myLog, OR_InputFormSubmit.lstState.getObjectName());
			
			DoAction.EnterText(wDriverWait, OR_InputFormSubmit.txtZipCode.getObjectSelector(), ZipCode, myLog,
					OR_InputFormSubmit.txtZipCode.getObjectName());
			DoAction.EnterText(wDriverWait, OR_InputFormSubmit.txtDomain.getObjectSelector(), WebSite, myLog,
					OR_InputFormSubmit.txtDomain.getObjectName());
			DoAction.Click(wDriver, wDriverWait, OR_InputFormSubmit.getDoYouHost(DoyouHost), myLog, "Do you host? = " + DoyouHost);
			
			DoAction.EnterText(wDriverWait, OR_InputFormSubmit.txtAreaProjDesc.getObjectSelector(), ProjDesc, myLog,
					OR_InputFormSubmit.txtAreaProjDesc.getObjectName());
			
			DoAction.Click(wDriver, wDriverWait, OR_InputFormSubmit.btnSend.getObjectSelector(), myLog, OR_InputFormSubmit.btnSend.getObjectName());

		}

	}
	@DataProvider
	public Object[][] InputFormData() throws RuntimeErrorException, FileNotFoundException, IOException{
		
		String[][] returnArray = null;
		ExcelObject exlObj = new ExcelObject(PrepareEnvironment.getTestDataMainPath() + "InputForm.xls");
		
		exlObj.setCurrentSheet("InputFormDemo");
		returnArray = exlObj.getTestCaseIDs();
		return returnArray;
	}
	
	/*
	 * Support functions
	 */

	private void doRadioButtonAction(By mainLocator, String buttonName, String gender, String expResult, Log myLog) {

		if (!gender.isEmpty()) {
			DoAction.Click(wDriver, wDriverWait, OR_RadioButtonsForm.getRaidoButtonLocator(mainLocator, gender), myLog,
					gender);
		}

		DoAction.Click(wDriver, wDriverWait, OR_RadioButtonsForm.getButtonLocator(
				OR_RadioButtonsForm.divRadioButtonDemo.getObjectSelector(), buttonName), myLog, buttonName);
		waitUntilLengthGreaterThanOne(OR_RadioButtonsForm.lblRadioButtonDemoResult.getObjectSelector());
		WebElement rbResult = wDriver.findElement(OR_RadioButtonsForm.lblRadioButtonDemoResult.getObjectSelector());

		if (expResult.equals(rbResult.getText())) {
			myLog.Pass("Radio button result is matched" + expResult);
		} else {
			myLog.Fail("Radio button result is not matched: Actual [" + rbResult.getText() + "] Expected [" + expResult
					+ "]");
		}
	}

	public void waitUntilLengthGreaterThanOne(By objLocator) {
		wDriverWait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.findElement(objLocator).getText().length() != 0;
			}
		});
	}

	public boolean ClickCheckBox(By GroupSelector, String labelText, Log myLog) {
		List<WebElement> arrWedEle = wDriver.findElements(GroupSelector);

		for (WebElement webElement : arrWedEle) {
			if (labelText.equals(webElement.getText())) {
				DoAction.Click(wDriver, wDriverWait, webElement, myLog, labelText);
				return true;
			}
		}
		myLog.Fail("Could not find checkbox with label as " + labelText);
		return false;
	}

	public boolean isCheckBoxSelected(WebElement checkBox) {
		return checkBox.isSelected();
	}

	/*
	 * Menu handler
	 */
	public boolean NavigateToScreen(String MenuName, String SubMenu, By expectedObjectLocator, Log myLog) {

		if (DoAction.Click(wDriver, wDriverWait, OR_HomePage.getMainMenuLocator(MenuName), myLog, MenuName)) {
			if (DoAction.Click(wDriver, wDriverWait, OR_HomePage.getSubMenuLocator(SubMenu), myLog, SubMenu)) {
				if (wDriverWait.until(ExpectedConditions.presenceOfElementLocated(expectedObjectLocator)) != null) {
					myLog.Pass("Navigation is successfull for " + MenuName + " >> " + SubMenu);
					return true;
				}
			}
		}
		myLog.Fail("Navigation is not successfull for " + MenuName + " >> " + SubMenu);
		return false;
	}

	private boolean matchResult(By objLocator, String expectedValue, String friendlyName, Log myLog) {
		WebElement lblResult = wDriver.findElement(objLocator);

		if (expectedValue.equals(lblResult.getText())) {
			myLog.Pass(friendlyName + " value is matched with " + expectedValue);
			return true;
		} else {
			myLog.Fail(friendlyName + " value is not matched with " + expectedValue + " and it displayed "
					+ lblResult.getText());
			return false;
		}

	}

	@AfterTest
	public void cleanUP() {

		wDriver.quit();
	}

}
