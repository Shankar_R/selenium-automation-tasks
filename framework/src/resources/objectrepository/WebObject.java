package objectrepository;

import org.openqa.selenium.By;

public class WebObject {
	private String ObjectName;
	private By ObjectSelector;

	public WebObject(String objName, By objSelector) {
		setObjectName(objName);
		setObjectSelector(objSelector);
	}

	public String getObjectName() {
		return ObjectName;
	}

	private void setObjectName(String objectName) {
		ObjectName = objectName;
	}

	public By getObjectSelector() {
		return ObjectSelector;
	}

	private void setObjectSelector(By objectSelector) {
		ObjectSelector = objectSelector;
	}

}
