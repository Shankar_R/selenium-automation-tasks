package objectrepository.seleniumeasy.InputForms;

import org.openqa.selenium.By;

import objectrepository.WebObject;

public class CheckboxesDemo {

	public WebObject lblHeading;
	public WebObject lblClickOnThisBoxResult;
	public WebObject btnCheckAll;
	public WebObject btnUnCheckAll;
	public WebObject cBokOptions;
	
	{
		lblHeading = new WebObject("Checkboxes page", By.xpath("//h3[contains(text(),'checkboxes')]"));
		lblClickOnThisBoxResult = new WebObject("Click on this Checkbox Result", By.id("txtAge"));
		btnCheckAll = new WebObject("Check All", By.xpath("//input[@value='Check All']"));
		btnUnCheckAll = new WebObject("Uncheck All", By.xpath("//input[@value='Uncheck All']"));
		cBokOptions = new WebObject("Option checkbox", By.xpath("//div[@class='checkbox']//label"));
	}
	
	
}
