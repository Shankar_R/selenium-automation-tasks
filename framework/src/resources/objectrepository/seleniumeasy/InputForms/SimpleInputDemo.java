package objectrepository.seleniumeasy.InputForms;

import org.openqa.selenium.By;

import objectrepository.WebObject;

public class SimpleInputDemo {

	public WebObject lblHeading;
	public WebObject txtEnterMessage;
	public WebObject btnShowMessage;
	public WebObject lblUserMessage;
	public WebObject txta;
	public WebObject txtb;
	public WebObject btnGetTotal;
	public WebObject lblTotalaPulsb;
	

	{
		lblHeading = new WebObject("Simple Input Form heading", By.xpath("//h3[contains(text(),'first example')]"));
		txtEnterMessage = new WebObject("Enter Message", By.xpath("//input[@id='user-message']"));
		btnShowMessage = new WebObject("Show Message", By.xpath("//button[text()='Show Message']"));
		lblUserMessage = new WebObject("User Message", By.xpath("//div[@id='user-message']/span"));
		txta = new WebObject("a value",By.xpath("//input[@id='sum1']"));
		txtb = new WebObject("a value",By.xpath("//input[@id='sum2']"));
		btnGetTotal = new WebObject("Get Total", By.xpath("//form[@id='gettotal']/button"));
		lblTotalaPulsb = new WebObject("Total a + b", By.xpath("//label[contains(text(),'Total a + b')]/following-sibling::span"));
	}
}
