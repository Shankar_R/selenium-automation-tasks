package objectrepository.seleniumeasy;

import org.openqa.selenium.By;

import objectrepository.WebObject;

public class HomePage {
	
	public WebObject lnkSimpleTestForm;
	public WebObject btnBasicExample;
	public WebObject lblBasicExamplepage;
	public WebObject mMainMenu;
	
	{	
		lnkSimpleTestForm = new WebObject("Simple Test Form link", By.xpath("//div[@id='basic']/div[@class='list-group']/a[1]"));
		btnBasicExample = new WebObject("Start Practicing", By.id("btn_basic_example"));
		lblBasicExamplepage = new WebObject("Basic example page", By.xpath("//div[@id=\"basic\"]/h3/span"));
		
		
		
		
	}
	
	
	public By getMainMenuLocator(String MainMenuName) {
		return By.xpath("//div[contains(@id,'navbar')]//ul//li/a[contains(text(),'" + MainMenuName + "')]");
	}
	//
	public By getSubMenuLocator(String SubMenuName) {
		return By.xpath("//ul[@class='dropdown-menu']//a[text()='" + SubMenuName + "']");
	}
}
